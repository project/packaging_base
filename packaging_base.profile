<?php

/**
 * Packaging Base profile provides a basic setup for Drupal packages.
 *
 */

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *   An array of modules to enable.
 */
function packaging_base_profile_modules() {
  return array(
  // Optional stuff
  'dblog', 'menu', 'path', 'php', 'update',

  // CORE D7
  'token', 'permissions_api',

  // cck
  'content', 'number', 'optionwidgets', 'text', 'content_copy', 'fieldgroup', 'nodereference', 'userreference',

  // cck addons
  'filefield', 'imagefield', 'link',

  // views
  'views', 'views_ui',

  // flag
  'flag',

  // rules
  'rules', 'rules_forms', 'rules_scheduler',
  
  // images
  'imageapi', 'imagecache', 'imagecache_ui', 'imageapi_gd',

  // administration (well, /admin/build/modules <i>will</i> be there...)
  // 'admin_menu', 'rules_admin',
  );
}

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile,
 *   and optional 'language' to override the language selection for
 *   language-specific profiles.
 */
function packaging_base_profile_details() {
  return array(
    'name' => 'Packaging Base',
    'description' => 'Packaging Base profile provides a basic setup for Drupal packages.'
  );
}

/**
 * Return a list of tasks that this profile supports.
 *
 * @return
 *   A keyed array of tasks the profile will perform during
 *   the final stage. The keys of the array will be used internally,
 *   while the values will be displayed to the user in the installer
 *   task list.
 */
function packaging_base_profile_task_list() {
/**
  return array(
    'some_task_id' => st('Description'),
  );
*/
}

/**
 * Perform any final installation tasks for this profile.
 *
 * For notes, see the default install profile
 */
function packaging_base_profile_tasks(&$task, $url) {
  // Insert default user-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st("A <em>page</em> is for creating and displaying information that rarely changes, such as an \"About us\" page of a website. A <em>page</em> does not allow visitor comments and is not featured on the site's initial home page."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
    array(
      'type' => 'article',
      'name' => st('Article'),
      'module' => 'node',
      'description' => st("An <em>article</em> is ideal for creating and displaying content that informs or engages website visitors. Press releases, site announcements, and informal blog-like entries may all be created with an <em>article</em> entry. An <em>article</em> is automatically featured on the site's initial home page, and provides visitors the ability to post comments."),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
      'help' => '',
      'min_word_count' => '',
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }  
  
  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));

  // misc
  variable_set(site_frontpage, 'home');

  // Update the menu router information.
  menu_rebuild();
  
  // clear the cache.
  cache_clear_all('variables', 'cache');
}

/**
 * Implementation of hook_form_alter().
 *
 * Allows the profile to alter the site-configuration form. This is
 * called through custom invocation, so $form_state is not populated.
 */
function packaging_base_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    // Set defaults
    $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
    $form['site_information']['site_mail']['#default_value'] = 'it@localhost';
    $form['admin_account']['account']['name']['#default_value'] = 'admin';
    $form['admin_account']['account']['mail']['#default_value'] = 'me@localhost';
    $form['admin_account']['account']['pass']['#default_value'] = 'z';
  }
}
